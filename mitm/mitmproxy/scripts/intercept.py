# intercept https with ROOT ca
# proxy trusted root CA -> pretends to be client with Pub Key?
from mitmproxy import http
import re
import json


def request(flow: http.HTTPFlow) -> None:
    """
    This example shows how to send a reply from the proxy immediately
    without sending any data to the remote server.
    """
    # flow.response.stream = True
    if flow.request.pretty_url == "https://www.binance.com/en":
        print("wrapper works, hook executes on ind page")
        # find a way to intercept https post
        # flow.response = http.HTTPResponse.make(
        #     200,  # (optional) status code
        #     b"middleware url",  # (optional) content
        #     {"Content-Type": "text/html"}  # (optional) headers
        # )


def response(flow: http.HTTPFlow):
    """
    Precompiled regular expression to separate
    http/https on dif ports accordingly 443 for ssh/tls, 80 basic http
    first predicate code execution means root CA trusted
    """
    if flow.request.url == "https://accounts.binance.com/gateway-api/v1/public/authcenter/login":
        print("AUTH POST request intercepted!")
        with decoded(flow.response):  # automatically decode gzipped responses.
            data = json.loads(flow.response.content)
            data["username"] = "anyUsername"
            flow.response.content = json.dumps(data)


if __name__ == '__main__':
    print("Notify working")


parse_host_header = re.compile(r"^(?P<host>[^:]+|\[.+\])(?::(?P<port>\d+))?$")


class Rerouter:
    def request(self, flow):
        if flow.client_conn.tls_established:
            flow.request.scheme = "https"
            sni = flow.client_conn.connection.get_servername()
            port = 443
        else:
            flow.request.scheme = "http"
            sni = None
            port = 80

        host_header = flow.request.host_header
        m = parse_host_header.match(host_header)
        if m:
            host_header = m.group("host").strip("[]")
            if m.group("port"):
                port = int(m.group("port"))

        flow.request.host_header = host_header
        flow.request.host = sni or host_header
        flow.request.port = port


addons = [Rerouter()]
